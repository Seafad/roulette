Roulette is an easy tool for creating customizable spinning wheels for random item selection :)

Simply tune RouletteController script at Roulette prefab to your needs, plug your graphics and you're all set to! Start by calling Roulette.Spin();