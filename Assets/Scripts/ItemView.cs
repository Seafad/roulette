﻿using UnityEngine;

public class ItemView : MonoBehaviour {

    [HideInInspector]
    public SpriteRenderer itemRenderer;

    public void SetItemSprite(Sprite itemSprite)
    {
        itemRenderer.sprite = itemSprite;
    }

}
