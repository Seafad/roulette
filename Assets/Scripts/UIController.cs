﻿using Model;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{

    public RouletteController roulette;
    public Button spinButton, okButton;
    public GameObject winPanel;
    public Image winImage;
    private Item selectedItem;

    void Start()
    {
        spinButton.onClick.AddListener(SpinButtonClick);
        okButton.onClick.AddListener(OkButtonClick);
        roulette.OnSpinComplete += OnSpinComplete;
    }

    void SpinButtonClick()
    {
        selectedItem = roulette.Spin();
    }

    void OkButtonClick()
    {
        winPanel.SetActive(!winPanel.activeSelf);
    }

    void OnSpinComplete()
    {
      
        winImage.sprite = selectedItem.itemSprite;
        winPanel.SetActive(!winPanel.activeSelf);
    }
}

