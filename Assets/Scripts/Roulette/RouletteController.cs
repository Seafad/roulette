﻿using Model;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using UnityEngine.Events;

public class RouletteController : MonoBehaviour
{

    public RouletteView view;
    public Action OnSpinComplete;
    [Range(0.1f, 10)]
    [HideInInspector]
    public float accelerationTime;
    [Range(1, 60)]
    [HideInInspector]
    public float spinTime;
    [Range(0.1f, 10)]
    [HideInInspector]
    public float decelerationTime;
    [Range(20, 360)]
    [HideInInspector]
    public float maxSpeed;
    [Range(4, 20)]
    [HideInInspector]
    public List<Item> items = new List<Item>();
    public bool bForceSelectItem;
    public Item forcedItem;

    private float sectorAngle;

    void Start()
    {
        Init();
    }

    /// <summary>
    /// Used to fill Roulette with items programmatically 
    /// </summary>
    public void InitWithItems(List<Item> items)
    {
        this.items = items;
        Init();
    }

    /// <summary>
    /// Starts roulette spinning
    /// </summary>
    /// <returns>Item that will be selected by roulette</returns>
    public Item Spin()
    {
        Item selectedItem;
        if (!bForceSelectItem)
            selectedItem = SelectRandomItem();
        else
            selectedItem = forcedItem;
        view.Spin(items.IndexOf(selectedItem));
        return selectedItem;
    }

    //Selects item from items list based on probability weights
    private Item SelectRandomItem()
    {
        float probabilitySum = items.Sum(i => i.probability);
        float p = UnityEngine.Random.Range(0, probabilitySum);
        float curr_probability = 0;
        foreach (Item item in items)
        {
            curr_probability += item.probability;
            if (p <= curr_probability)
            {
                return item;
            }

        }
        //TODO: replace Debug.Log with inspector warning
        Debug.LogWarning("List shouldn't be empty");
        return new Item();
    }

    private void Init()
    {
        if (bForceSelectItem)
            items.Add(forcedItem);
        sectorAngle = 360f / items.Count;
        view.InitLayout(items, sectorAngle);
    }

    public void OnFinish()
    {
        OnSpinComplete();
    }
}
