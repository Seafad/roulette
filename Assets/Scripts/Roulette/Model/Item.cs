﻿using System;
using UnityEngine;

namespace Model
{
    [Serializable]
    public struct Item
    {
        public Sprite itemSprite;
        [Range(0, 1)]
        public float probability;
    }
}
