﻿using System.Collections.Generic;
using Model;
using UnityEngine;
using DG.Tweening;

public class RouletteView : MonoBehaviour
{

    public float separatorMargin;
    public bool IsSpinning { get { return isSpinning; } private set { } }
    [HideInInspector]
    [SerializeField]
    private Transform wheel;
    [HideInInspector]
    [SerializeField]
    private SpriteRenderer background;
    [HideInInspector]
    [SerializeField]
    private GameObject separatorPrefab, itemHolderPrefab;
    [HideInInspector]
    [SerializeField]
    private Transform separatorsDir, itemsDir;
    [HideInInspector]
    [SerializeField]
    private RouletteController controller;

    private int itemToSelectIndex;
    private int dir;
    private float sectorAngle;
    private Vector3 separatorOffset;
    private Vector3 itemOffset;
    private float wheelHeight;
    private float speed;
    private bool isSpinning;
    private float curr_time, accel_rate, decel_rate;
    private byte state;
    private Vector3 endRotation;

    void Start()
    {
        wheelHeight = background.bounds.size.y;
        separatorOffset = new Vector3(0, separatorMargin);
        itemOffset = new Vector3(0, wheelHeight * (3f / 8));
        isSpinning = false;
    }

    //Thanks to alexchanyan for awesome 16x16 pixel item pack :) https://alexchanyan.itch.io/16x16-rpg-item-pack
    //Sets up separators for wheel segments and rotates the wheel by half-segment 
    internal void InitLayout(List<Item> items, float sectorAngle)
    {
        this.sectorAngle = sectorAngle;
        for (int i = 0; i < items.Count; i++)
        {
            Quaternion rotation = Quaternion.Euler(0, 0, sectorAngle * i);
            GameObject item = Instantiate(itemHolderPrefab, rotation * itemOffset, rotation, itemsDir);
            item.GetComponent<ItemView>().SetItemSprite(items[i].itemSprite);
            rotation *= Quaternion.Euler(0, 0, sectorAngle / 2);
            Instantiate(separatorPrefab, rotation * separatorOffset, rotation, separatorsDir);


        }
    }

    internal void Spin(int itemToSelect)
    {
        if (isSpinning) return;
        isSpinning = true;
        dir = -1;
        this.itemToSelectIndex = itemToSelect;
        endRotation = new Vector3(0, 0, -sectorAngle * itemToSelect);
        curr_time = 0;
        accel_rate = controller.maxSpeed / controller.accelerationTime;
        state = 0;
    }

    void Update()
    {
        if (isSpinning)
        {
            switch (state)
            {
                case 0:
                    wheel.Rotate(Vector3.forward, dir * speed * Time.deltaTime);
                    curr_time += Time.deltaTime;
                    speed += accel_rate * Time.deltaTime;
                    if (curr_time >= controller.accelerationTime)
                    {
                        curr_time = 0;
                        speed = controller.maxSpeed;
                        state = 1;
                    }
                    break;
                case 1:
                    wheel.Rotate(Vector3.forward, dir * speed * Time.deltaTime);
                    curr_time += Time.deltaTime;
                    if (curr_time >= controller.spinTime)
                    {
                        curr_time = 0;
                        state = 2;
                    }
                    break;
                case 2:
                    wheel.DORotate(endRotation, controller.decelerationTime, RotateMode.Fast).OnComplete(OnComplete);
                    state = 3;
                    break;
                case 3:
                    break;
            }
        }
    }

    void OnComplete()
    {
        Debug.Log("Complete");
        isSpinning = false;
        controller.OnFinish();
    }

}
