﻿using Model;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor(typeof(RouletteController))]
public class RouletteEditor : Editor
{

    private SerializedProperty accelTimeProp;
    private SerializedProperty spinTimeProp;
    private SerializedProperty decelTimeProp;
    private SerializedProperty itemsProp;
    private SerializedProperty maxSpeedProp;
    private ReorderableList list;
    private RouletteController script
    {
        get
        {
            return target as RouletteController;
        }
    }
    private static GUIContent accelTimeLabel = new GUIContent("Раскрутка (сек)");
    private static GUIContent spinTimeLabel = new GUIContent("Вращение (сек)");
    private static GUIContent decelTimeLabel = new GUIContent("Торможение (сек)");
    private static GUIContent maxSpeedLabel = new GUIContent("Макс скорость (гр/с)");

    private void OnEnable()
    {
        accelTimeProp = serializedObject.FindProperty("accelerationTime");
        spinTimeProp = serializedObject.FindProperty("spinTime");
        decelTimeProp = serializedObject.FindProperty("decelerationTime");
        itemsProp = serializedObject.FindProperty("items");
        maxSpeedProp = serializedObject.FindProperty("maxSpeed");
        list = new ReorderableList(serializedObject, itemsProp, false, true, true, true);
        // This could be used aswell, but I only advise this your class inherrits from UnityEngine.Object or has a CustomPropertyDrawer
        // Since you'll find your item using: serializedObject.FindProperty("list").GetArrayElementAtIndex(index).objectReferenceValue
        // which is a UnityEngine.Object
        // reorderableList = new ReorderableList(serializedObject, serializedObject.FindProperty("list"), true, true, true, true);

        // Add listeners to draw events
        list.drawHeaderCallback += DrawHeader;
        list.drawElementCallback += DrawElement;

        list.onAddCallback += AddItem;
        list.onRemoveCallback += RemoveItem;
    }

    private void OnDisable()
    {
        list.drawHeaderCallback -= DrawHeader;
        list.drawElementCallback -= DrawElement;

        list.onAddCallback -= AddItem;
        list.onRemoveCallback -= RemoveItem;
    }

    private void DrawHeader(Rect rect)
    {
        GUI.Label(rect, "Вещи: Спрайт - Вроятность выпадения");
    }

    private void DrawElement(Rect rect, int index, bool active, bool focused)
    {
        var item = itemsProp.GetArrayElementAtIndex(index);
        EditorGUI.PropertyField(new Rect(rect.x, rect.y, rect.width / 2 - 20, rect.height), item.FindPropertyRelative("itemSprite"), GUIContent.none);
        EditorGUI.PropertyField(new Rect(rect.width / 2, rect.y, rect.width / 2, rect.height), item.FindPropertyRelative("probability"), GUIContent.none);
    }

    private void AddItem(ReorderableList list)
    {
        if (list.count < 12)
        {
            script.items.Add(new Item());
            EditorUtility.SetDirty(target);
        }
    }

    private void RemoveItem(ReorderableList list)
    {
        if (list.count > 2)
        {
            script.items.RemoveAt(list.index);
            EditorUtility.SetDirty(target);
        }
    }


    public override void OnInspectorGUI()
    {
        EditorGUILayout.PropertyField(accelTimeProp, accelTimeLabel);
        EditorGUILayout.PropertyField(spinTimeProp, spinTimeLabel);
        EditorGUILayout.PropertyField(decelTimeProp, decelTimeLabel);
        EditorGUILayout.PropertyField(maxSpeedProp, maxSpeedLabel);
        list.DoLayoutList();
        serializedObject.ApplyModifiedProperties();
        DrawDefaultInspector();
    }
}
